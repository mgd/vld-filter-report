# README #

`vld-filter-report` is a tool for filtering output from Visual Leak Detector found at https://vld.codeplex.com/.

As a remedy for a proper README file, here is the help output from the program:

``` console
$ vld-filter-report -h
usage: vld-filter-report [-h] [-V] [-e ENC] [-c COLNAME [COLNAME ...]]
                         [-s COLNAME [COLNAME ...]] [-S STRING [STRING ...]]
                         [-p PREFIX [PREFIX ...]] [-r REGEX [REGEX ...]]
                         [FILE [FILE ...]]

positional arguments:
  FILE                  Files to read, if empty, stdin is used

optional arguments:
  -h, --help            show this help message and exit
  -V, --version         show program's version number and exit
  -e ENC, --input-encoding ENC
                        Select the encoding to use when reading input. This
                        option takes no effect for stdin. Default input
                        encoding is 'utf-8'.
  -c COLNAME [COLNAME ...], --show-columns COLNAME [COLNAME ...]
                        Select which columns to show and in which order. The
                        possible columns are: 'block' (block number), 'hash'
                        (block hash), 'address' (memory address), 'thread'
                        (thread id), 'count' (duplicate count of this
                        allocation), 'bytes' (bytes allocated in this block),
                        'total' (total bytes in this and duplicates), 'total-
                        hr' (same as 'total' but in human-readable format),
                        'frame' (stack frame where allocation took place, see
                        '-S' below). The default is to show all columns in the
                        order they are mentioned above.
  -s COLNAME [COLNAME ...], --sort-by-columns COLNAME [COLNAME ...]
                        Sort by columns in the in the order specified. Numeric
                        fields are sorted in descending order whereas all
                        other fields are sorted in ascending order. Frames are
                        sorted by the function name following the '!' mark in
                        the frame. Default is to sort by the 'total' column
                        only.
  -S STRING [STRING ...], --skip-frames-containing STRING [STRING ...]
                        Skip frames in the stack trace when reporting the
                        topmost stack frame. A frame will be skipped if it
                        contains any of the STRING arguments given.
  -p PREFIX [PREFIX ...], --strip-frame-prefix PREFIX [PREFIX ...]
                        Strip prefixes from the topmost stack frame before
                        reporting it. The topmost frame (after skipping
                        insignificant frames) is searched for each PREFIX
                        argument given in the order they are passed. If a
                        prefix matches the beginning of the frame, it is
                        removed before the search continues with the next
                        PREFIX argument. Searching for prefixes takes place
                        _before_ searching for regexes (see option '-r').
  -r REGEX [REGEX ...], --strip-frame-regex REGEX [REGEX ...]
                        Strip string matched by regexes from the topmost stack
                        frame before reporting it. The topmost frame (after
                        skipping insignificant frames) is searched for each
                        REGEX argument given in the order they are passed. If
                        a regex matches the frame, the matching part of the
                        frame is removed before the search continues with the
                        next REGEX argument. Searching for regexes takes place
                        _after_ searching for prefixes (see option '-p').
```
